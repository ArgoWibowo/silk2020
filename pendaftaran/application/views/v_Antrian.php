<div class="content-wrapper">
	 <section class="content-header">
      <h1>
        Dashboard Pasien
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard Pasien</li>
      </ol>
    </section>

    <section class="content">

    <button class="btn btn-primay" type="button" data-toggle="modal" data-target="#exampleModal"  > <i class="fa fa-dashboard"></i> Tambah Data Antrian
    </button>
      <table class="table">
      	<tr>
      		<th>No Antrian</th>
      		<th>No Rekam Medis</th>
            <th>Dokter</th>
      		<th>Waktu</th>
      		<th>Hari</th>
    	
      	</tr>


      <?php 
      	$no =1;
        if(isset($antrians)){
            foreach ($antrians as $row){
                echo "<tr style='text-align:center';>";
                echo "<td>".$no++."</td>";
                echo "<td>".$no_antrian=$row->no_antrian."</td>";
                echo "<td>".$no_rm=$row->no_rm."</td>";
                echo "<td>".$id_dokter=$row->id_dokter."</td>";
                echo "<td>".$waktu=$row->waktu."</td>";
                echo "<td>".$hari=$row->hari."</td>";
            	echo "</tr>";
        }
        }else{
            echo "Tidak Ada Data Yang Ditampilkan !!!";
        } 

        ?>

        
      </table>
    </section>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Form Antrian</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      <form>
      <div class="form-group">
        <label for="exampleInputEmail1">No Rekam Medis</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Nama Lengkap</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Poliklinik</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Dokter</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
      </div>
        <div class="form-group">
        <label for="exampleInputEmail1">Waktu</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
      </div>
      </div>
        <div class="form-group">
        <label for="exampleInputEmail1">No Antrian</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
      </div>
      </form>


        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

</div>