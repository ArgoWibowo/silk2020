<div class="content-wrapper">
	 <section class="content-header">
      <h1>
        Dashboard Pasien
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard Pasien</li>
      </ol>
    </section>

    <section class="content">

    <button class="btn btn-primay" type="button" data-toggle="modal" data-target="#exampleModal"  > <i class="fa fa-dashboard"></i> Tambah Data Pasien
    </button>
      <table class="table">
      	<tr>
      		<th>No</th>
      		<th>Nama</th>
          <th>Tempat Lahir</th>
      		<th>Tanggal</th>
      		<th>Umur</th>
      		<th>Agama</th>
      		<th>Pekerjaan</th>
      		<th>Alamat</th>
      		<th>Darah</th>
      		<th>Status</th>
      		<th>Pendidikan</th>
      		<th>JK</th>
          <th>Nama Ibu</th>
      		<th>Kewernegaraan</th>
      		<th>Pembayaran</th> 	
      	</tr>


      <?php 
      	$no =1;
        if(isset($pasiens)){
            foreach ($pasiens as $row){
                echo "<tr style='text-align:center';>";
                echo "<td>".$no++."</td>";
                echo "<td>".$id_pasien=$row->id_pasien."</td>";
                echo "<td>".$nama=$row->nama."</td>";
                echo "<td>".$tempat_lhr=$row->tempat_lhr."</td>";
                echo "<td>".$tgl_lahir=$row->tgl_lahir."</td>";
                echo "<td>".$agama=$row->agama."</td>";
                echo "<td>".$pekerjaan=$row->pekerjaan."</td>";
                echo "<td>".$alamat=$row->alamat."</td>";
                echo "<td>".$gol_darah=$row->gol_darah."</td>";
                echo "<td>".$sts_perkawinan=$row->sts_perkawinan."</td>";
                echo "<td>".$pendidikan_terakhir=$row->pendidikan_terakhir."</td>";
                echo "<td>".$jk=$row->jk."</td>";
                echo "<td>".$nama_ibu=$row->nama_ibu."</td>";
                echo "<td>".$kewernegaraan=$row->kewernegaraan."</td>";
                echo "<td>".$cara_pembayaran=$row->cara_pembayaran."</td>";
            	echo "</tr>";
        }
        }else{
            echo "Tidak Ada Data Yang Ditampilkan !!!";
        } 

        ?>

        
      </table>
    </section>

    <!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Form Tambah Pasien</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      <form>
      <div class="form-group">
        <label for="exampleInputEmail1">Nama Pasien</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Tempat Lahir</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
      </div>
      
      <div class="form-group">
        <label for="exampleInputEmail1">Tanggal Lahir</label>
 

      <input type="date" id="start" name="trip-start"
       value="2018-07-22"
       min="2018-01-01" max="2018-12-31">

      </div>
       <fieldset class="form-group">
     <label for="exampleInputEmail1">Agama</label>
    <div class="row">
      
      <div class="col-sm-10">
        <div class="form-check">
          <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
          <label class="form-check-label" for="gridRadios1">
            Hindu
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2" checked>
          <label class="form-check-label" for="gridRadios2">
            Budha
          </label>
        </div>
        <div class="form-check disabled">
          <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" value="option3" checked>
          <label class="form-check-label" for="gridRadios3">
            Komhucu
          </label>
        </div>
         <div class="form-check disabled">
          <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" value="option3" checked>
          <label class="form-check-label" for="gridRadios3">
            Kristen
          </label>
        </div>
         <div class="form-check disabled">
          <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" value="option3" checked>
          <label class="form-check-label" for="gridRadios3">
            Katolik
          </label>
        </div>
        <div class="form-check disabled">
          <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" value="option3" checked>
          <label class="form-check-label" for="gridRadios3">
            Islam
          </label>
        </div>
      </div>
    </div>
  </fieldset>
      <div class="form-group">
        <label for="exampleInputEmail1">Pekerjaan</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Alamat</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
      </div>

      <fieldset class="form-group">
     <label for="exampleInputEmail1">Golongan Darah</label>
    <div class="row">
      
      <div class="col-sm-10">
        <div class="form-check">
          <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
          <label class="form-check-label" for="gridRadios1">
            A
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2">
          <label class="form-check-label" for="gridRadios2">
            B
          </label>
        </div>
        <div class="form-check disabled">
          <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" value="option3" disabled>
          <label class="form-check-label" for="gridRadios3">
            AB
          </label>
        </div>
         <div class="form-check disabled">
          <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" value="option3" disabled>
          <label class="form-check-label" for="gridRadios3">
            O
          </label>
        </div>
      </div>
    </div>
  </fieldset>
    
      <div class="form-group">
        <label for="exampleInputEmail1">Status</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Pendidikan Terakhir</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Jenis Kelamin</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Nama Ibu</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Warga Negara</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Pembayaran</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
      </div>
      </form>


        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

</div>