<html>
  <head>
    <title>CRUD Product</title>
  </head>
  <body>
    <h1>Data Product</h1>
    <hr>
    <a href='<?php echo base_url("product/tambah"); ?>'>Tambah Data</a><br><br>
    <table border="1" cellpadding="7">
      <tr>
        <th>Kode Barang</th>
        <th>Nama</th>
        <th>Harga</th>
        <th>Image</th>
        <th>Keterangan</th>
        <th colspan="2">Aksi</th>
      </tr>
      <?php
      if( ! empty($products)){ 
        foreach($products as $prdct){
          echo "<tr>
          <td>".$prdct->product_id."</td>
          <td>".$prdct->name."</td>
          <td>".$prdct->price."</td>
          <td>".$prdct->image."</td>
          <td>".$prdct->description."</td>
          <td><a href='".base_url("siswa/ubah/".$prdct->product_id)."'>Ubah</a></td>
          <td><a href='".base_url("siswa/hapus/".$prdct->product_id)."'>Hapus</a></td>
          </tr>";
        }
      }else{ // Jika data siswa kosong
        echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
      }
      ?>
    </table>
  </body>
</html>