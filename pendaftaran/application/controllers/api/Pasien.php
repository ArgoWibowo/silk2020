<?php
use Restserver\Libraries\REST_Controller;
require(APPPATH . 'libraries/REST_Controller.php');
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien extends REST_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pasien_model', 'pasien');
        $this->load->library('form_validation');
        $this->BASE_API="http://localhost/pendaftaran/";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    public function index_get(){
        $data['pasiens'] = json_decode($this->curl->simple_get($this->BASE_API.'/pasien'));
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('v_Pasien', $data);
        $this->load->view('templates/footer');
    }

    // public function index_get()
    // {
    //     $id = $this->get('id_pasien');
    //     if ($id === NULL)
    //     {
    //         $pasien = $this->pasien->getPasien();
    //     } else {
    //         $pasien = $this->pasien->getPasien($id);
    //     }
    //     if ($pasien) {
    //          $this->response([
    //                 'status' => TRUE,
    //                 'data' => $pasien
    //             ], REST_Controller::HTTP_OK);
    //     } else {
    //         $this->response([
    //                 'status' => FALSE,
    //                 'message' => 'Product ID Tidak Ditemukan'
    //             ], REST_Controller::HTTP_NOT_FOUND);
    //     }
    //     $this->load->view('templates/header');
    //     $this->load->view('templates/sidebar');
    //     $this->load->view('v_Pasien', $id);
    //     $this->load->view('templates/footer');

    // }
    

    // public function index_delete()
    // {
    //     $id = $this->delete('id_pasien');

    //     if ($id === NULL) {
    //         $this->response([
    //                 'status'    => FALSE,
    //                 'message'   => 'Tidak Ada Yang Dihapus'
    //             ], REST_Controller::HTTP_BAD_REQUEST);   
    //     } else {
    //         if ($this->pasien->deletePasien($id) > 0) 
    //         {
    //              $this->response([
    //                 'status'    => TRUE,
    //                 'id'        => $id_pasien,
    //                 'message'   => 'Telah Dihapus.'
    //             ], REST_Controller::HTTP_NO_CONTENT);
    //         } else {
    //             $this->response([
    //                 'status'  => FALSE,
    //                 'message' => 'Kode Pasien Tidak Ditemukan'
    //             ], REST_Controller::HTTP_BAD_REQUEST);      
    //         } 
    //     }
    // } 

     public function index_post()
     {
        $input = $this->input->post();
        $this->db->insert('pasien', $input);

      
        $this->response(['Product Berhasil Dibuat.'],REST_Controller::HTTP_OK); 

     }


     public function index_put() {
        $id_pasien = $this->put('id_pasien');
        $data = array(
                    'id_pasien'             => $this->put('id_pasien'),
                    'nama'                  => $this->put('nama'),
                    'tempat_lhr'            => $this->put('tempat_lhr'),
                    'tgl_lahir'             => $this->put('tgl_lahir'),
                    'umur'                  => $this->put('umur'),
                    'agama'                 => $this->put('agama'),
                    'nama'                  => $this->put('pekerjaan'),
                    'price'                 => $this->put('alamat'),
                    'image'                 => $this->put('gol_darah'),
                    'sts_perkawinan'        => $this->put('sts_perkawinan'),
                    'pendidikan_terakhir'   => $this->put('pendidikan_terakhir'),
                    'jk'                    => $this->put('jk'),
                    'nama_ibu'              => $this->put('nama_ibu'),
                    'image'                 => $this->put('kewernegaraan'),
                    'description'           => $this->put('cara_pembayaran'),
                    );

        $this->db->where('id_pasien', $id_pasien);
        $update = $this->db->update('pasien', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

}