<?php
use Restserver\Libraries\REST_Controller;
require(APPPATH . 'libraries/REST_Controller.php');
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends REST_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Product_model', 'product');
        $this->load->library('form_validation');
        $this->BASE_API="http://localhost/pendaftaran/";
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->helper('url');
	}

	public function index_get()
	{
		$id = $this->get('product_id');
		if ($id === NULL)
        {
        	$product = $this->product->getProduct();
        } else {
        	$product = $this->product->getProduct($id);
        }
		if ($product) {
			 $this->response([
                    'status' => TRUE,
                    'data' => $product
                ], REST_Controller::HTTP_OK);
		} else {
			$this->response([
                    'status' => FALSE,
                    'message' => 'Product ID Tidak Ditemukan'
                ], REST_Controller::HTTP_NOT_FOUND);
		}

	}
    


	public function index_delete()
    {
        $id = $this->delete('product_id');

        if ($id === NULL) {
            $this->response([
                    'status' 	=> FALSE,
                    'message'	=> 'Tidak Ada Yang Dihapus'
                ], REST_Controller::HTTP_BAD_REQUEST);   
        } else {
        	if ($this->product->deleteProduct($id) > 0) 
        	{
        		 $this->response([
                    'status' 	=> TRUE,
                    'id'	 	=> $product_id,
                    'message' 	=> 'Telah Dihapus.'
                ], REST_Controller::HTTP_NO_CONTENT);
        	} else {
        		$this->response([
                    'status'  => FALSE,
                    'message' => 'ID Tidak Ditemukan'
                ], REST_Controller::HTTP_BAD_REQUEST);   	
        	} 
    	}
  	} 

  	 public function index_post()
  	 {
        $input = $this->input->post();
        $this->db->insert('products', $input);

      
        $this->response(['Product Berhasil Dibuat.'],REST_Controller::HTTP_OK); 

   	 }


     public function index_put() {
        $product_id = $this->put('product_id');
        $data = array(
                    'product_id'           => $this->put('product_id'),
                    'name'                  => $this->put('name'),
                    'price'                 => $this->put('price'),
                    'image'                 => $this->put('image'),
                    'description'           => $this->put('description'),
                    );

        $this->db->where('product_id', $product_id);
        $update = $this->db->update('products', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

}