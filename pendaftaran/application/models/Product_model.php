<?php  

/**
 * 
 */
class Product_model extends CI_Model
{

	public function getProduct($id = null)
	{
		if ($id === null)
		{
			return $this->db->get('products')->result_array();
		} else {
			return $this->db->get_where('products', ['product_id' => $id])->result_array();
		}
	}

	// public function getProduct(){
 //    return $this->db->get('products')->result();
 //  	}


	public function deleteProduct($id)
	{
		$this->db->delete('products', ['product_id' => $id]);
		return $this->db->affected_rows();
	} 

	// public function createProduct($data)
	// {
	// 	$this->db->insert('products', $data);
	// 	return $this->db->affected_rows();
	// } 

	public function createProduct($data,$table){
		$this->db->insert($table,$data);
		return $this->db->affected_rows();
	}

	// public function updateProduct($data, $id)
	// {
	// 	$this->db->update('products', '$data' ['product_id' => $id]);
	// 	return $this->db->affected_rows();
	// } 

	public	function updateProduct(){
		return $this->db->get('products');
	}

}
