<?php
class Obat_model extends CI_Model {
    public function get()
    {
        return $this->db->get('obat')->result_array();
    }

    public function getId($id)
    {
        return $this->db->get_where('obat',['id_obat'=>$id])->result_array();
    }
    public function add($nama,$golongan,$harga, $stok, $expired,$created)
    {
        $data = [
            'nama_obat' => $nama,
            'golongan' => $golongan,
            'harga' => $harga,
            'stok' => $stok,
            'expired' => $expired,
            'created_at' => $created
        ];
    
        $this->db->insert('obat',$data);
        return $this->db->affected_rows();
    }
    public function delete($id)
    {
        $this->db->delete('obat',['id_obat'=>$id]);
        return $this->db->affected_rows();
    }
    public function edit($id,$nama,$golongan,$harga, $stok, $expired,$update)
    {
        $this->db->set('nama_obat',$nama);
        $this->db->set('golongan',$golongan);
        $this->db->set('harga',$harga);
        $this->db->set('stok',$stok);
        $this->db->set('expired',$expired);
        $this->db->set('updated_at',$update);
        $this->db->where('id_obat',$id);
        $this->db->update('obat');
        return $this->db->affected_rows();
    }
}