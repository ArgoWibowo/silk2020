<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Obat extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('Obat_model');
    }

    public function obat_get()
    {
        $obat=$this->Obat_model->get();

        $id = $this->get('id');

        if ($id === NULL)
        {
            if ($obat)
            {
                $this->response($obat, REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        }
        else {
            if ($id <= 0)
            {
                $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST);
            }

            if (!empty($id))
            {
                $getobat=$this->Obat_model->getId($id);
                if(count($getobat)>0){
                    $this->set_response($getobat, REST_Controller::HTTP_OK);
                }
                else{
                    $this->set_response([
                        'status' => FALSE,
                        'message' => 'raono slur'
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
            }
            else
            {
                $this->set_response([
                    'status' => FALSE,
                    'message' => 'User could not be found'
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        }
    }

    public function obat_post()
    {
       $nama=$this->post('nama');
       $golongan=$this->post('golongan');
       $harga=$this->post('harga');
       $stok=$this->post('stok');
       $expired=$this->post('expired');
       $created=date("Y-m-d h:i:sa");
       $cek = $this->Obat_model->add($nama,$golongan,$harga, $stok, $expired,$created);
       if($cek>0){
           $message="jos gan";
           $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
       }else{
           $message="rakalap";
           $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // CREATED (201) being the HTTP response code
       }
    }
    public function obat_put()
    {
        $id=$this->put('id');
        $nama=$this->put('nama');
       $golongan=$this->put('golongan');
       $harga=$this->put('harga');
       $stok=$this->put('stok');
       $expired=$this->put('expired');
       $update=date("Y-m-d h:i:sa");
       $cek = $this->Obat_model->edit($id,$nama,$golongan,$harga, $stok, $expired,$update);
        if($cek>0){
            $message="mantab slur";
            $this->set_response($message, REST_Controller::HTTP_CREATED);
        }else{
            $message="rakalap";
            $this->set_response($message, REST_Controller::HTTP_NO_CONTENT);
        }
    }
    public function obat_delete()
    {
        $id = (int) $this->delete('id');
        if ($id <= 0)
        {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST);
        }
        $delete=$this->Obat_model->delete($id); 
        if($delete>0)
        {
            $message = 'jos kehapus';
            $this->set_response($message, REST_Controller::HTTP_OK);
        }else{
            $message = 'ra dadi';
            $this->set_response($message, REST_Controller::HTTP_NO_CONTENT);
        }  
    }

}
