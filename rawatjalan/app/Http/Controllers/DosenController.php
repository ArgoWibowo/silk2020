<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; //Pustaka Database 

class DosenController extends Controller
{
public function index() {
        $mhs = DB::table('dosen')->paginate(10);
        return view('dosen', ['mhs'=>$mhs]);
    }
}