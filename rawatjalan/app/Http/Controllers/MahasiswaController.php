<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; //Pustaka Database 

class MahasiswaController extends Controller
{
    public function index() {
        $mhs = DB::table('mahasiswa')->paginate(10);
        return view('mahasiswa', ['mhs'=>$mhs]);
    }
    public function tambah() {
        return view('tambah');
    }
    public function simpan(Request $request) {
        $bhs = implode(",", $request->get('bahasa')); //Implode untuk menggabungkan string  
        DB::table('mahasiswa')->insert([
        'nim' => $request->nim,
        'nama' => $request->nama,
        'jenis_kelamin' => $request->jenis_kelamin,
        'jurusan' => $request->jurusan,
        'bahasa' => $bhs
        ]);
        return redirect('/mahasiswa');
}
public function edit($id) {
    $mhs = DB::table('mahasiswa')->where('nim', $id )->get();
    return view('edit', ['mhs'=>$mhs]);
}
public function update(Request $request) {
    $bhs = implode(",", $request->get('bahasa'));
    DB::table('mahasiswa')->where('nim', $request->id)->update([
    'nim' => $request->nim,
    'nama' => $request->nama,
    'jenis_kelamin' => $request->jenis_kelamin,
    'jurusan' => $request->jurusan,
    'bahasa' => $bhs
    ]);
    return redirect('/mahasiswa');
}

public function delete($id) {
    $mhs = DB::table('mahasiswa')->where('nim', $id)->delete();
    return redirect('/mahasiswa');
}

 public function search(Request $request) {
        $cari = $request->q;
        $mhs = DB::table('mahasiswa')
        ->where('nama','like',"%".$cari."%")
        ->paginate();
        return view('mahasiswa',['mhs' => $mhs]);
    }

}

