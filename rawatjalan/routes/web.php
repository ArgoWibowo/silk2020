<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/mahasiswa','MahasiswaController@index');

Route::get('/dosen','DosenController@index');

Route::get('/mahasiswa/tambah','MahasiswaController@tambah');

Route::post('/mahasiswa/simpan','MahasiswaController@simpan');

Route::get('/mahasiswa/edit/{id}','MahasiswaController@edit');

Route::post('/mahasiswa/update','MahasiswaController@update');

Route::get('/mahasiswa/delete/{id}','MahasiswaController@delete');

Route::get('/mahasiswa/search','MahasiswaController@search');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
