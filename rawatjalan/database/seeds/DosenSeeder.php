<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker; 

class DosenSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        
                for ($i = 1; $i <= 50; $i++) {
                DB::table('dosen')->insert([ //tabel sesuai dengan database dan method yang digunakan insert 
                    'nidn' => $faker->numberBetween(7217000,7217800),
                    'nama' => $faker->name,
                    'jenis_kelamin' => $faker->numberBetween(1,0),
                    'jurusan' => $faker->jobTitle,
                    'bahasa' => $faker->country
            ]);
    }
}
}
