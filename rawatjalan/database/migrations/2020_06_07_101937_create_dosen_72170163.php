<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDosen72170163 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('dosen')->insert([ //tabel sesuai dengan database dan method yang digunakan insert 
            'nidn' => '000002', //Isi data yang akan dikirimkan  
            'nama' => 'Yetli Oslan',
            'jenis_kelamin' => 'Perempuan',
            'jurusan' => 'Sistem Informasi',
            'bahasa' => 'Bahasa Indonesia'
        ]);
        DB::table('dosen')->insert([ //tabel sesuai dengan database dan method yang digunakan insert 
           'nidn' => '0000011', //Isi data yang akan dikirimkan  
           'nama' => 'Budi Sutedjo',
           'jenis_kelamin' => 'Laki-Laki',
           'jurusan' => 'Sistem Informasi',
           'bahasa' => 'Bahasa Indonesia,Inggris,Mandarin'
         
    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dosen_72170163');
    }
}
