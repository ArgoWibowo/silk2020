
@extends('layouts.app')

@section('content')

<nav class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="col-md-12">
                            <div class="mainmenu">
                                <div class="navbar navbar-nobg">
                                    <div class="navbar-header">
                                        <a class="navbar-brand" href="https://www.ukdw.ac.id/">
                                            <img src="{{ asset('img/ukdw.png') }}" style="max-height: 50px; margin-top: -10px;">
                                    </div>
                
                <a class="navbar-brand" href="#">Puskesmas Sejahtera</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a> 
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Data
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="/mahasiswa">Data Antrian Pasien</a>
                        <a class="dropdown-item" href="/dosen">Data Dokter </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Laboratorium </a>
                        </div>
                        <li class="nav-item active">
                        <a class="nav-link" href="#">Contact <span class="sr-only">(current)</span></a> 
                    </li> 
        </nav>
<nav class="container"> 
<title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <center> <h4>Informasi Puskesmas</h4>
 <p>Jl. Cik Di Tiro No.30
Open 24 hours · (0274) 563333</p> </center>

  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="/w3images/lights.jpg" target="_blank">
          <img src="/img/2.jpeg" alt="Lights" style="width:100%">
          <div class="caption">
           <center> <p>Private hospital in Yogyakarta, Indonesia. It was founded in 1929 by five sisters from St. Carolus Borromeus. </p> </center>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="/w3images/nature.jpg" target="_blank">
          <img src="/img/10.jpeg" alt="Nature" style="width:100%">
          <div class="caption">
          <center>  <p>Stay at home if you feel unwell. If you have a fever, cough and difficulty breathing, seek medical attention and call in advance. Follow the directions of your local health authority.</p> </center>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="/w3images/fjords.jpg" target="_blank">
          <img src="/img/11.jpeg" alt="Fjords" style="width:100%">
          <div class="caption">
            <center> <p>I would say this is the best hospital in Yogyakarta for my family. It has very clean rooms and cozy enough. It also has many garden outside that could calm you down. The staffs including the doctors and nurses are very friendly and helpful. </p> </center>
          </div>
        </a>
      </div>
    </div>
  </div>
</div>

<body>
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
<h4>Dokumentasi </h4>  
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
    <li data-target="#myCarousel" data-slide-to="4"></li>
    <li data-target="#myCarousel" data-slide-to="5"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="/img/4.jpeg" alt="Los Angeles" style="width:100%;">
    </div>

    <div class="item">
      <img src="/img/6.jpeg" alt="Chicago" style="width:100%;">
    </div>
  
    <div class="item">
      <img src="/img/3.jpeg" alt="New york" style="width:100%;">
    </div>

    <div class="item">
      <img src="/img/4.jpeg" alt="New york" style="width:100%;">
    </div>

    <div class="item">
      <img src="/img/5.jpeg" alt="New york" style="width:100%;">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>

</body>



@endsection
