<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SIA UKDW</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
<nav class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">SIA UKDW</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a> 
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#">About</a>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Data
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="/mahasiswa">Data Mahasiswa</a>
        <a class="dropdown-item" href="/dosen">Data Dosen </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#">Bimbingan </a>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#">Contact</a>
    </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
</div>
    </nav>

    <blockquote class="blockquote">
     <p class="mt-3">Data Pasien</p>
     </blockquote>
     <blockquote class="blockquote">
    </blockquote> 
    
    @foreach($mhs as $m)
    @php 
        $bhs = explode(',', $m->bahasa); 
    @endphp
        <form method="post" action="/mahasiswa/update"> 
        @php echo csrf_field() @endphp   
        <input type="hidden" class="form-control" name="id" id="id" value="@php echo $m->nim @endphp" readonly>
    <div class="form-group">
        <label for="exampleFormControlInput1">NIM</label>
        <input type="number" class="form-control" name="nim" id="nim" value="@php echo $m->nim @endphp">
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput1">Nama Mahasiswa</label>
        <input type="text" class="form-control" name="nama" id="nama"  value="@php echo $m->nama @endphp">
    </div>

        <div class="form-check">
    <input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin" value="Laki-Laki" @php if(($m->jenis_kelamin)=='Laki-Laki') echo 'checked' @endphp>
    <label class="form-check-label" for="exampleRadios1">
       Laki-Laki
    </label> 
    </div> 
    <div class="form-check">
    <input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan" @php if(($m->jenis_kelamin)=='Perempuan') echo 'checked' @endphp>
    <label class="form-ch eck-label" for="exampleRadios2">
        Perempuan
    </label>
    </div>
  
<br>

    <select class="custom-select" name="jurusan" >
        <option selected>Pilih Jurusan </option>
        <option value="Teknik Informatika"  @php if(($m->jurusan)=='Teknik Informatika') echo 'selected' @endphp>Teknik Informatika</option>
        <option value="Sistem Informasi" @php if(($m->jurusan)=='Sistem Informasi') echo 'selected' @endphp>Sistem Informasi </option>

    </select>
    
    <div class="form-group">
        </div>
            <div class="form-check">
            <input class="form-check-input" type="checkbox" name="bahasa[]" value="Indonesia" <?php in_array('Indonesia',$bhs) ? print 'checked':'' ?>>   
            <label class="form-check-label" for="defaultCheck1">
            Indonesia
        </label>
    </div>
    <div class="form-group">
        </div>
            <div class="form-check">
            <input class="form-check-input" type="checkbox" name="bahasa[]" value="Inggris" <?php in_array('Inggris',$bhs) ? print 'checked':'' ?>>   
            <label class="form-check-label" for="defaultCheck1">
            Inggris 
        </label>
        </div>
        <div class="form-group">
        </div>
            <div class="form-check">
            <input class="form-check-input" type="checkbox" name="bahasa[]" value="Mandarin" <?php in_array('Mandarin',$bhs) ? print 'checked':'' ?>>    
            <label class="form-check-label" for="defaultCheck1">
            Mandarin
        </label>
    </div>
    <br> 
    <div class="form-group">
    <input type="submit" class="btn btn-primary" value="simpan">
    </form>
    @endforeach
    </div>

    <br> 
</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>