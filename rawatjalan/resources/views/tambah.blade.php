<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Pasien</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
<nav class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Puskesmas Sejahtera</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
    </li> 
    <li class="nav-item">
        <a class="nav-link" href="#">About</a>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Data
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="/mahasiswa">Data Pasien</a>
        <a class="dropdown-item" href="#">Data Dokter </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#">Laboratorium </a>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#">Contact</a>
    </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
</div>
    </nav>

    <blockquote class="blockquote">
     <p class="mt-3">Data Pasien</p>
     </blockquote>
     <blockquote class="blockquote">
    </blockquote> 
        <form method="post" action="/mahasiswa/simpan">
        @php echo csrf_field() @endphp 
    <div class="form-group">
        <label for="exampleFormControlInput1">ID</label>
        <input type="number" class="form-control" name="nim" id="nim" placeholder="ID">
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput1">Nama Pasien</label>
        <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap">
    </div>

        <div class="form-check">
    <input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin" value="Laki-Laki" checked>
    <label class="form-check-label" for="exampleRadios1">
      Laki Laki
    </label> 
    </div>
    <div class="form-check">
    <input class="form-check-input" type="radio" name="jenis_kelamin" id="jenis_kelamin" value="Perempuan">
    <label class="form-ch eck-label" for="exampleRadios2">
       Perempuan
    </label>
    </div>
  
<br>

    <select class="custom-select" name="jurusan">
        <option selected>Jenis Penyakit </option>
        <option value="Teknik Informatika">Malaria</option>
        <option value="Sistem Informasi"> Demam </option>
    </select>
    
    <div class="form-group">
        </div>
            <div class="form-check">
            <input class="form-check-input" type="checkbox" name="bahasa[]" value="Indonesia"> 
            <label class="form-check-label" for="defaultCheck1">
            Indonesia
        </label>
    </div>
    <div class="form-group">
        </div>
            <div class="form-check">
            <input class="form-check-input" type="checkbox" name="bahasa[]" value="Inggris"> 
            <label class="form-check-label" for="defaultCheck1">
            Inggris
        </label>
        </div>
        <div class="form-group">
        </div>
            <div class="form-check">
            <input class="form-check-input" type="checkbox" name="bahasa[]" value="Mandarin"> 
            <label class="form-check-label" for="defaultCheck1">
            Mandarin
        </label>
    </div>
    <br> 
    <div class="form-group">
    <input type="submit" class="btn btn-primary" value="Simpan">
    </form>
    </div>
    <br> 
</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>