<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Puskesmas Sejahtera</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
<nav class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Sistem Informasi Rawat Jalan</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
            <a class="nav-link" href="/home">Home <span class="sr-only">(current)</span></a> 
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">About</a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Data
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/mahasiswa">Data Antrian Pasien</a>
            <a class="dropdown-item" href="/dosen">Data Dokter </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Laboratorium </a>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Contact</a>
        </li>
        </ul>
        <form class="form-inline my-2 my-lg-0" method="get" action="/mahasiswa/search">
        <input class="form-control mr-sm-2" name="q" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
    </nav>

        <blockquote class="blockquote">
    <p class="mt-3">Data Pasien </p>
    </blockquote>
    <a class="btn btn-primary" href="mahasiswa/tambah" role="button">Data Pasien[+]</a>
    <blockquote class="blockquote">
    </blockquote> 
        <table class="table">
    <thead>
        <tr>
        <th scope="col">No  </th>
        <th scope="col">ID </th>
        <th scope="col">Nama Pasien</th>
        <th scope="col">Jenis Kelamin</th>
        <th scope="col">Jenis Penyakit</th>
        <th scope="col">Asal Daerah </th>
        <th scope="col">Aksi</th>
        </tr>
    </thead>
    <!--@php $no=1 @endphp--> 
    @foreach($mhs as $no => $m) 
    <tbody>
        <tr>
        <th scope="row"><?php echo ++$no + ($mhs->currentPage()-1)*$mhs->perPage() ?></th>
        <td>@php echo $m->nim @endphp</td>
        <td>@php echo $m->nama @endphp</td>
        <td>@php echo $m->jenis_kelamin @endphp</td>
        <td>@php echo $m->jurusan @endphp</td>
        <td>@php echo $m->bahasa @endphp</td>
        <td>
            <a href="/mahasiswa/edit/@php echo $m->nim @endphp">Edit  </a> |
            <a href="/mahasiswa/delete/@php echo $m->nim @endphp">Delete </a>
        </td>
        </tr>
    </tbody>
    @endforeach
    </table>
    Jumlah Data : @php echo $mhs->total() @endphp
    @php echo $mhs->links() @endphp
    </nav>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>